package core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import model.Tweet;

import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.Location;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.endpoint.Location.*;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.BasicClient;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import com.twitter.hbc.twitter4j.Twitter4jStatusClient;

import utils.BoundingBox;
import utils.TwitterCredentials;

public class TwitHunter {

	private static final int NUM_PROCESSING_THREADS = 4;

	private static BoundingBox bbox;
	private static TwitterCredentials credentials;
	private static File file;
	private static FileWriter fw;
	private static BufferedWriter bw;
	/*
	 * Loads the bounding box to crawl from "locations" file
	 */
	private static void loadLocation() throws IOException {

		BufferedReader br = new BufferedReader(new FileReader("locations"));
		String line = br.readLine();
		String coordinatesString = line.substring(("locations=").length(),
				line.length());

		String[] coordinates = coordinatesString.split(",");

		double lon1 = Double.parseDouble(coordinates[0].trim());
		double lon2 = Double.parseDouble(coordinates[2].trim());
		double lat1 = Double.parseDouble(coordinates[1].trim());
		double lat2 = Double.parseDouble(coordinates[3].trim());

		bbox = new BoundingBox(lat1, lon1, lat2, lon2);

		br.close();

	}

	/*
	 * Loads the twitter credentials to be used from "credentials" file
	 */
	private static void loadCredentials() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("credentials"));

		String consumerKey = new String(br.readLine());
		String consumerSecret = new String(br.readLine());

		String token = new String(br.readLine());
		String tokenSecret = new String(br.readLine());

		credentials = new TwitterCredentials(consumerKey, consumerSecret,
				token, tokenSecret);
		br.close();

	}

	private static StatusListener listener = new StatusListener() {
		// Fired when a new tweet has been recieved
		public void onStatus(Status status) {
			Tweet tweet = new Tweet(status);
			System.out.println(tweet.toJSONString());
			try {
				bw.write(tweet.toJSONString()+",\n");
				bw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
		}

		public void onTrackLimitationNotice(int limit) {
		}

		public void onScrubGeo(long user, long upToStatus) {
		}

		public void onException(Exception e) {
		}
	};

	public static void oauth(String consumerKey, String consumerSecret,
			String token, String secret) throws InterruptedException {

		// Create an appropriately sized blocking queue
		BlockingQueue<String> queue = new LinkedBlockingQueue<String>(10000);

		// Define our endpoint: By default, delimited=length is set (we need
		// this for our processor)
		// and stall warnings are on.
		StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint();

		List<Location> locationsList = new ArrayList<Location>();
		// FORMAT: lonSW, latSW, lonNE, latNE
		Coordinate SW = new Coordinate(bbox.getLon1(), bbox.getLat1());
		Coordinate NE = new Coordinate(bbox.getLon2(), bbox.getLat2());
		Location l = new Location(SW, NE);
		locationsList.add(l);

		// Sets the filter to the query
		endpoint.addPostParameter("locations",
				SW.longitude() + "," + SW.latitude() + "," + NE.longitude()
						+ "," + NE.latitude());

		Authentication auth = new OAuth1(consumerKey, consumerSecret, token,
				secret);
		// Authentication auth = new BasicAuth(username, password);

		// Create a new BasicClient. By default gzip is enabled.
		BasicClient client = new ClientBuilder().hosts(Constants.STREAM_HOST)
				.endpoint(endpoint).authentication(auth)
				.processor(new StringDelimitedProcessor(queue)).build();

		// Create an executor service which will spawn threads to do the actual
		// work of parsing the incoming messages and
		// calling the listeners on each message
		ExecutorService service = Executors
				.newFixedThreadPool(NUM_PROCESSING_THREADS);

		// Wrap our BasicClient with the twitter4j client
		Twitter4jStatusClient t4jClient = new Twitter4jStatusClient(client,
				queue, Lists.newArrayList(listener), service);

		// Establish a connection
		t4jClient.connect();
		System.out.println("Connection done!");

		// Start each thread to compute recieved messages
		for (int threads = 0; threads < NUM_PROCESSING_THREADS; threads++) {

			// This must be called once per processing thread
			t4jClient.process();
		}

	}

	public static void main(String[] args) {
		try {
			// Loads the BoundingBox
			loadLocation();

			// Loads the credentials to be used
			loadCredentials();

			// Create the JSON file where we will save tweets
			createJSONFile();

			// Connects to the API and crawls the info
			oauth(credentials.getConsumerKey(),
					credentials.getConsumerSecret(), credentials.getToken(),
					credentials.getTokenSecret());

		} catch (IOException e) {
			System.err.println("Error reading configuration file");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void createJSONFile() throws IOException {
		Calendar cal = Calendar.getInstance();
		cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String s = "tweets" + sdf.format(cal.getTime()) + ".json";
		System.out.println(s + " created.");
		file = new File(s);
		 
		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		fw = new FileWriter(file.getAbsoluteFile());
		bw = new BufferedWriter(fw);
	}

}
