package model;

import java.util.Date;

import com.google.gson.Gson;

import twitter4j.GeoLocation;
import twitter4j.Status;

public class Tweet {
	
	private long id;
	private long userId;
	
	private Date createdAt;
	
	private long inReplyToUserId;
	private String text;
	private GeoLocation geoLocation;
	
	private String userLang;	
	private String userLocation;
	
	
	public Tweet(Status status){
		this.id = status.getId();
		this.userId = status.getUser().getId();
		
		this.createdAt = status.getCreatedAt();
		
		this.inReplyToUserId = status.getInReplyToUserId();
		this.text = status.getText();
		this.geoLocation = status.getGeoLocation();
		
		this.userLang = status.getUser().getLang();
		this.userLocation = status.getUser().getLocation();
	}


	@Override
	public String toString() {
		return "Tweet [id=" + id + ", userId=" + userId + ", createdAt="
				+ createdAt + ", inReplyToUserId=" + inReplyToUserId
				+ ", text=" + text + ", geoLocation=" + geoLocation
				+ ", userLang=" + userLang + ", userLocation=" + userLocation
				+ "]";
	}
	
	public String toJSONString(){
		Gson gson = new Gson();
		
		String json = gson.toJson(this);
		
		return json;
	}
}