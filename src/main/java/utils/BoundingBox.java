package utils;
/**
 * Created with IntelliJ IDEA.
 * User: jaranda
 * Date: 12/06/13
 * Time: 15:15
 */
public class BoundingBox {

    private double lat1;

    private double lon1;

    private double lat2;

    private double lon2;

    public BoundingBox(double lat1, double lon1, double lat2, double lon2) {
        this.lat1 = lat1;
        this.lon1 = lon1;
        this.lat2 = lat2;
        this.lon2 = lon2;
    }

    public double getLat1() {
        return lat1;
    }

    public void setLat1(double lat1) {
        this.lat1 = lat1;
    }

    public double getLon1() {
        return lon1;
    }

    public void setLon1(double lon1) {
        this.lon1 = lon1;
    }

    public double getLat2() {
        return lat2;
    }

    public void setLat2(double lat2) {
        this.lat2 = lat2;
    }

    public double getLon2() {
        return lon2;
    }

    public void setLon2(double lon2) {
        this.lon2 = lon2;
    }
    
    public boolean isPointInside(double lat, double lon){
    	if(lat>this.lat1 && lat < this.lat2 && lon > this.lon1 && lon < this.lon2){
    		return true;
    	}
    	else{
    		return false;
    	}
    }

}
